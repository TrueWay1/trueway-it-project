// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.domain;


import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "UC_ShoppingCartPosition_Product_And_ShoppingCart", columnNames = { "product_id", "shopping_cart_id"} )
})
public class ShoppingCartPosition implements Positionable {

    @Id @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_shopping_cart_2_product"))
    private Product product;

    private int quantity;

    public ShoppingCartPosition(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public ShoppingCartPosition() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
