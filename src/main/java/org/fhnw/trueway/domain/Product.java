// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.domain;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(uniqueConstraints = {
		@UniqueConstraint(name = "UC_Product_Code", columnNames = "code")
})
public class Product {
	
	@Id @GeneratedValue(strategy = IDENTITY)
	private Long id;

	@NotNull
	private String code;

	@NotNull
	private String name;

	@Column(columnDefinition="TEXT")
	private String description;
	@NotNull
	@Min(0) @Max(10_000)
	private BigDecimal price;

	@NotNull
	private boolean available;


	@NotNull
	private int maxNumberOfProducts;

	@NotNull
	private BigDecimal minPalletSpaces;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getMaxNumberOfProducts() {
		return maxNumberOfProducts;
	}

	public void setMaxNumberOfProducts(int maxNumberOfProducts) {
		this.maxNumberOfProducts = maxNumberOfProducts;
	}

	public BigDecimal getMinPalletSpaces() {
		return minPalletSpaces;
	}

	public void setMinPalletSpaces(BigDecimal minPalletSpaces) {
		this.minPalletSpaces = minPalletSpaces;
	}
}
