// Davide Seabra

package org.fhnw.trueway.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Embeddable
public class Address {

    @NotNull
    @Column(nullable = false) // TODO: Embeddable and @NotNull were not working on db
    private String street;

    @NotNull
    @Min(1000) @Max(9658)
    private int zipCode;

    @NotNull
    @Column(nullable = false) // TODO: Embeddable and @NotNull were not working on db
    private String city;


    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
