// Gazmend Shefiu

package org.fhnw.trueway.domain;

public interface Positionable {
    int getQuantity();
    Product getProduct();
}
