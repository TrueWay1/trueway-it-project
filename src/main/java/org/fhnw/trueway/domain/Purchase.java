// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.domain;

import javax.persistence.*;
import java.util.Collection;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Purchase {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @OneToMany
    @JoinColumn(name = "purchase_id", nullable = false)
    private Collection<PurchasePosition> purchasePositions;

    @ManyToOne
    @JoinColumn(insertable = false, updatable = false)
    private User user;

    private boolean shipped;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<PurchasePosition> getPurchasePositions() {
        return purchasePositions;
    }

    public void setPurchasePositions(Collection<PurchasePosition> purchasePositions) {
        this.purchasePositions = purchasePositions;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isShipped() {
        return shipped;
    }

    public void setShipped(boolean shipped) {
        this.shipped = shipped;
    }
}
