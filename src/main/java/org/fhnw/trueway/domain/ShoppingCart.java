// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.domain;

import javax.persistence.*;
import java.util.Collection;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class ShoppingCart {

    @Id @GeneratedValue(strategy = IDENTITY)
    private Long id;


    @OneToMany(cascade = ALL)
    @JoinColumn(name = "shopping_cart_id", nullable = false)
    private Collection<ShoppingCartPosition> shoppingCartPositions;

    @OneToOne(mappedBy = "shoppingCart", optional = false)
    @PrimaryKeyJoinColumn
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<ShoppingCartPosition> getShoppingCartPositions() {
        return shoppingCartPositions;
    }

    public void setShoppingCartPositions(Collection<ShoppingCartPosition> shoppingCartPositions) {
        this.shoppingCartPositions = shoppingCartPositions;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
