// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


// H2 at the moment in memory
// https://www.baeldung.com/spring-boot-h2-database
// Swagger-UI (https://springdoc.org/)
// http://localhost:8080/swagger-ui.html
// http://localhost:8080/v3/api-docs

@SpringBootApplication
public class TruewayApplication  {

	public static void main(String[] args) {
		SpringApplication.run(TruewayApplication.class, args);
	}

}
