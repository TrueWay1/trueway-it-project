// Davide Seabra

package org.fhnw.trueway.api.mapper;

import org.fhnw.trueway.api.dto.ProductDto;
import org.fhnw.trueway.domain.Product;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    List<ProductDto> mapList(List<Product> products);
    ProductDto map(Product entity);

    @InheritInverseConfiguration
    Product map(ProductDto dto);
}
