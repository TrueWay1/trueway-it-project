// Davide Seabra

package org.fhnw.trueway.api.mapper;

import org.fhnw.trueway.api.dto.AddressDto;
import org.fhnw.trueway.domain.Address;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    AddressDto map(Address source);

    @InheritInverseConfiguration
    Address map(AddressDto source);
}
