// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.api.mapper;


import org.fhnw.trueway.api.dto.ShoppingCartDto;
import org.fhnw.trueway.domain.ShoppingCart;
import org.mapstruct.*;

import java.util.Collection;

@Mapper(
        componentModel ="spring",
        uses = {ShoppingCartPositionsMapper.class}
)
public interface ShoppingCartMapper {
    Collection<ShoppingCartDto> mapList(Collection<ShoppingCart> shoppingCarts);

    @Mapping(target = "shippingCost", expression = "java(java.math.BigDecimal.valueOf(org.fhnw.trueway.utils.ShippingCalculator.calculateShippingCost(entity)))")
    ShoppingCartDto map(ShoppingCart entity);

    @InheritInverseConfiguration
    @BeanMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    ShoppingCart map(ShoppingCartDto dto);

}
