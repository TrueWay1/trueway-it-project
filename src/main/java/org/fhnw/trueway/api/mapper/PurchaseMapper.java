// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.api.mapper;


import org.fhnw.trueway.api.dto.PurchaseDto;
import org.fhnw.trueway.domain.Purchase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(
        componentModel ="spring",
        uses = {PurchasePositionsMapper.class}
)
public interface PurchaseMapper {

    Collection<PurchaseDto> mapList(Collection<Purchase> purchases);

    @Mapping(target = "shippingCost", expression = "java(java.math.BigDecimal.valueOf(org.fhnw.trueway.utils.ShippingCalculator.calculateShippingCost(entity)))")
    PurchaseDto map(Purchase entity);

    Purchase map(PurchaseDto dto);

}
