// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.api.mapper;

import org.fhnw.trueway.api.dto.PurchasePositionDto;
import org.fhnw.trueway.domain.PurchasePosition;
import org.mapstruct.*;

import java.math.BigDecimal;
import java.util.Collection;

@Mapper(componentModel = "spring")
public interface PurchasePositionsMapper {
    Collection<PurchasePositionDto>  mapList(Collection<PurchasePosition> purchasePositions);

    @Mapping(target = "productName", source = "product.name")
    @Mapping(target = "productPrice", source = "product.price")
    @Mapping(target = "numberOfProducts", source = "quantity")
    PurchasePositionDto map(PurchasePosition entity);

    @AfterMapping
    default void calculateTotalPalletSpace(PurchasePosition entity, @MappingTarget PurchasePositionDto dto) {
        BigDecimal quantity = BigDecimal.valueOf(entity.getQuantity());
        BigDecimal palletSpace = entity.getProduct().getMinPalletSpaces();
        dto.setTotalPalletSpace(palletSpace.multiply(quantity));
    }

    @AfterMapping
    default void calculateTotalPrice(PurchasePosition entity, @MappingTarget PurchasePositionDto dto){
        BigDecimal quantity = BigDecimal.valueOf(entity.getQuantity());
        BigDecimal price = entity.getProduct().getPrice();
        dto.setTotalPrice(price.multiply(quantity));
    }

    @InheritInverseConfiguration
    PurchasePosition map(PurchasePositionDto dto);

}
