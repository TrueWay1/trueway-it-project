// Davide Seabra

package org.fhnw.trueway.api.mapper;

import org.fhnw.trueway.api.dto.UserDto;
import org.fhnw.trueway.domain.User;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(
        componentModel = "spring",
        uses = { AddressMapper.class, ShoppingCartMapper.class, PurchaseMapper.class }
)
public interface UserMapper {
     List<UserDto> mapList(List<User> user);
     UserDto map(User user);

     @InheritInverseConfiguration
     User map(UserDto dto);
}
