// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.api.mapper;


import org.fhnw.trueway.api.dto.ShoppingCartPositionDto;
import org.fhnw.trueway.domain.ShoppingCartPosition;
import org.mapstruct.*;

import java.math.BigDecimal;
import java.util.Collection;

@Mapper(componentModel = "spring", uses = ProductMapper.class)
public interface ShoppingCartPositionsMapper {
    Collection<ShoppingCartPositionDto> mapList(Collection<ShoppingCartPosition> shoppingCartPositions);

    @Mapping(target = "productQuantity", source = "quantity")
    ShoppingCartPositionDto map(ShoppingCartPosition entity);

    @AfterMapping
    default void calculateTotalPrice(ShoppingCartPosition entity, @MappingTarget ShoppingCartPositionDto dto){
        BigDecimal quantity = BigDecimal.valueOf(entity.getQuantity());
        BigDecimal price = entity.getProduct().getPrice();
        dto.setTotalPrice(price.multiply(quantity));
    }


    @InheritInverseConfiguration
    ShoppingCartPosition map(ShoppingCartPositionDto dto);
}
