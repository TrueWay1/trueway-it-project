// Davide Seabra

package org.fhnw.trueway.api.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class ProductDto {
	private Long id;

	@NotNull
	private String code;

	@NotNull
	private String name;
	private String description;

	@NotNull
	@Min(0) @Max(10_000)
	private BigDecimal price;

	@NotNull
	private boolean available;

	@NotNull
	private int maxNumberOfProducts;

	@NotNull
	private BigDecimal minPalletSpaces;


	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("code", code)
				.append("name", name)
				.append("price", price)
				.append("available", available)
				.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getMaxNumberOfProducts() {
		return maxNumberOfProducts;
	}

	public void setMaxNumberOfProducts(int maxNumberOfProducts) {
		this.maxNumberOfProducts = maxNumberOfProducts;
	}

	public BigDecimal getMinPalletSpaces() {
		return minPalletSpaces;
	}

	public void setMinPalletSpaces(BigDecimal minPalletSpaces) {
		this.minPalletSpaces = minPalletSpaces;
	}
}
