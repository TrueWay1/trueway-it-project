// Davide Seabra

package org.fhnw.trueway.api.dto;

import java.math.BigDecimal;
import java.util.Collection;

public class PurchaseDto {

    private Long id;

    private Collection<PurchasePositionDto> purchasePositions;

    private boolean shipped;

    private BigDecimal shippingCost;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<PurchasePositionDto> getPurchasePositions() {
        return purchasePositions;
    }

    public void setPurchasePositions(Collection<PurchasePositionDto> purchasePositions) {
        this.purchasePositions = purchasePositions;
    }

    public boolean isShipped() {
        return shipped;
    }

    public void setShipped(boolean shipped) {
        this.shipped = shipped;
    }

    public BigDecimal getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(BigDecimal shippingCost) {
        this.shippingCost = shippingCost;
    }
}
