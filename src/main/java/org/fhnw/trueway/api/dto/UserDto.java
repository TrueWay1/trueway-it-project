// Davide Seabra

package org.fhnw.trueway.api.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Collection;

public class UserDto {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String surname;

    @Valid
    private AddressDto address;

    @NotNull
    @Email
    private String email;
    
    @NotNull
    private String password;

    @NotNull
    private BigDecimal distance = BigDecimal.ZERO;

    private ShoppingCartDto shoppingCart;

    private Collection<PurchaseDto> purchases;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public ShoppingCartDto getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCartDto shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Collection<PurchaseDto> getPurchases() {
        return purchases;
    }

    public void setPurchases(Collection<PurchaseDto> purchases) {
        this.purchases = purchases;
    }
}
