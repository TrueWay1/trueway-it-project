// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.api.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

public class ShoppingCartDto {

    private Long id;

    private Collection<ShoppingCartPositionDto> shoppingCartPositions;

    private BigDecimal shippingCost;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<ShoppingCartPositionDto> getShoppingCartPositions() {
        return shoppingCartPositions;
    }

    public BigDecimal getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(BigDecimal shippingCost) {
        this.shippingCost = shippingCost;
    }

    public void setShoppingCartPositions(Collection<ShoppingCartPositionDto> shoppingCartPositions) {
        if (null == shoppingCartPositions) {
            this.shoppingCartPositions = new ArrayList<>();
        } else {
            this.shoppingCartPositions = shoppingCartPositions;
        }
    }
    public BigDecimal getTotalPrice() {
        BigDecimal positionCostSum = new BigDecimal(shoppingCartPositions.stream()
                .map(ShoppingCartPositionDto::getTotalPrice)
                .mapToDouble(tp -> tp.doubleValue())
                .sum());
        BigDecimal shippingCost = getShippingCost();
        return positionCostSum.add(shippingCost);
    }
}
