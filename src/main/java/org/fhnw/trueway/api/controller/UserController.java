// Davide Seabra & Gazmend Shefiu

package org.fhnw.trueway.api.controller;

import org.fhnw.trueway.api.dto.PurchaseDto;
import org.fhnw.trueway.api.dto.ShoppingCartDto;
import org.fhnw.trueway.api.dto.UserDto;
import org.fhnw.trueway.api.mapper.PurchaseMapper;
import org.fhnw.trueway.api.mapper.ShoppingCartMapper;
import org.fhnw.trueway.api.mapper.UserMapper;
import org.fhnw.trueway.api.request.PostShoppingCartRequest;
import org.fhnw.trueway.business.service.ShoppingCartService;
import org.fhnw.trueway.business.service.UserService;
import org.fhnw.trueway.domain.ShoppingCart;
import org.fhnw.trueway.domain.ShoppingCartPosition;
import org.fhnw.trueway.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
    
    private final UserService userService;
    private final ShoppingCartService shoppingCartService;
    private final UserMapper userMapper;
    private final ShoppingCartMapper shoppingCartMapper;
    private final PurchaseMapper purchaseMapper;


    public UserController(UserService userService, ShoppingCartService shoppingCartService, UserMapper userMapper, ShoppingCartMapper shoppingCartMapper, PurchaseMapper purchaseMapper) {
        this.userService = userService;
        this.shoppingCartService = shoppingCartService;
        this.userMapper = userMapper;
        this.shoppingCartMapper = shoppingCartMapper;
        this.purchaseMapper = purchaseMapper;
    }

    @GetMapping("user")
    public List<UserDto> getUser(){
        LOG.info("ENTER: UserController#getUser()");
        List<User> users = userService.findAllUser();
        List<UserDto> dtoList = userMapper.mapList(users);
        return dtoList;
    }

    @GetMapping("/user/{id}")
    public UserDto getUserById(@PathVariable("id") Long id) {
        LOG.info("ENTER: UserController#getUserById(" + id +")");
        Optional<User> optUser = userService.findUserById(id);

        if (optUser.isPresent()) {
            return userMapper.map(optUser.get());
        } else {
            return null;
        }
    }

    @GetMapping("/user/{id}/shoppingcart")
    public ShoppingCartDto getUserShoppingCartById(@PathVariable("id") Long id) {
        LOG.info("ENTER: UserController#getUserShoppingCartById(" + id + ")");
        Optional<User> optUser = userService.findUserById(id);

        if (optUser.isPresent()) {
            return shoppingCartMapper.map(optUser.get().getShoppingCart());
        } else {
            return null;
        }
    }


    // TODO: Reorganize Logic here
    @PostMapping("/user/{id}/shoppingcart")
    public ShoppingCartDto postUserShoppingCartById(
            @PathVariable("id") Long id,
            @RequestBody PostShoppingCartRequest request
    ) {
        LOG.info("ENTER: UserController#postUserShoppingCartById(" + id + ", " + request + ")");
        Optional<User> optUser = userService.findUserById(id);

        if (optUser.isPresent()) {
            User user = optUser.get();
            ShoppingCart foundShoppingCart = user.getShoppingCart();

            Optional<ShoppingCartPosition> optShoppingCart = getProductFromShoppingCart(foundShoppingCart, request.getProductId());
            if (optShoppingCart.isPresent()) { // Update Position
                ShoppingCartPosition shoppingCartPosition = optShoppingCart.get();
                shoppingCartPosition.setQuantity(shoppingCartPosition.getQuantity() + request.getQuantity());
            } else {
                shoppingCartService.addNewShoppingCartPos(id, request.getProductId(), request.getQuantity());
            }
        }

        return getUserShoppingCartById(id);
    }

    private Optional<ShoppingCartPosition> getProductFromShoppingCart(ShoppingCart shoppingCart, Long productId) {
        return shoppingCart.getShoppingCartPositions().stream()
                .filter(pos -> pos.getProduct().getId().equals(productId))
                .findFirst();
    }


    @GetMapping("/user/{id}/purchase")
    public Collection<PurchaseDto>getUserPurchasesById(@PathVariable ("id") Long id){
        LOG.info("ENTER: UserController#getUserPurchaseById(" + id + ")");
        Optional<User> optUser = userService.findUserById(id);

        if (optUser.isPresent()){
            return purchaseMapper.mapList(optUser.get().getPurchases());
        }
        else {
            return Collections.emptyList();
        }
    }

    @GetMapping("/user/email/{email}")
    public UserDto getUserByEmail(@PathVariable("email") String email) {
        LOG.info("ENTER: UserController#getUserByEmail(" + email + ")");
        Optional<User> optUser = userService.findUserByEmail(email);

        if (optUser.isPresent()) {
            return userMapper.map(optUser.get());
        } else {
            return null;
        }
    }

    @PostMapping("/user")
    public UserDto saveUser(@RequestBody @Valid UserDto dto) {
        LOG.info("ENTER: UserController#saveUser(" + dto + ")");
        User entity = userMapper.map(dto);
        User persisted = userService.insertUser(entity);
        return userMapper.map(persisted);
    }

    @PutMapping ("/user/{id}")
    public UserDto putUser(
            @PathVariable("id") Long id,
            @RequestBody @Valid UserDto dto) {

        LOG.info("ENTER: UserController#putUser(" + id + ", " + dto + ")");
        User entity = userMapper.map(dto);

        Optional <User> optUser = userService.updateUser(id, entity);
        if (optUser.isPresent()) {
            return userMapper.map(optUser.get());
        } else {
            return null;
        }
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable Long id) {
        LOG.info("ENTER: UserController#deleteUser(" + id + ")");
        userService.deleteUserById(id);
    }

}



