// Davide Seabra

package org.fhnw.trueway.api.controller;

import org.fhnw.trueway.api.dto.ProductDto;
import org.fhnw.trueway.api.mapper.ProductMapper;
import org.fhnw.trueway.business.service.ProductService;
import org.fhnw.trueway.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {
	private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);

	private final ProductService productService; // Singleton bean

	private final ProductMapper mapper;

	public ProductController(ProductService productService, ProductMapper mapper) {
		this.productService = productService;
		this.mapper = mapper;
	}

	@GetMapping("/product")
	public List<ProductDto> getProducts() {
		LOG.info("ENTER: ProductController#getProducts()");
		List<Product> products = productService.findAllProducts();
		List<ProductDto> dtoList = mapper.mapList(products);
		return dtoList;
	}

	@GetMapping("/product/{id}")
	public ProductDto getProduct(@PathVariable("id") Long id) {
		LOG.info("ENTER: ProductController#getProduct(" + id + ")");

		Optional<Product> optProduct = productService.findProductById(id);

		if (optProduct.isPresent()) {
			return mapper.map(optProduct.get());
		} else {
			return null;
		}
	}

	@PostMapping("/product")
	public ProductDto saveProduct(@RequestBody @Valid ProductDto dto) {
		LOG.info("ENTER: ProductController#saveProduct(" + dto + ")");

		Product entity = mapper.map(dto);
		Product persisted = productService.insertProduct(entity);
		return mapper.map(persisted);
	}

	@PutMapping("/product/{id}")
	public ProductDto putProduct(
			@PathVariable("id") Long id,
			@RequestBody @Valid ProductDto dto
	) {
		LOG.info("ENTER: ProductController#putProduct(" + id + "," + dto + ")");

		Product entity = mapper.map(dto);

		Optional<Product> optProduct = productService.updateProduct(id, entity);
		if (optProduct.isPresent()) {
			return mapper.map(optProduct.get());
		} else {
			return null;
		}
	}

	@DeleteMapping("/product/{id}")
	public void deleteProduct(@PathVariable Long id) {
		LOG.info("ENTER: ProductController#deleteProduct(" + id + ")");
		productService.deleteProductById(id);
	}

}
