// Davide Seabra

package org.fhnw.trueway.api.controller;


import org.fhnw.trueway.api.dto.PurchaseDto;
import org.fhnw.trueway.api.mapper.PurchaseMapper;
import org.fhnw.trueway.business.service.PurchaseService;
import org.fhnw.trueway.domain.Purchase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@RestController
public class PurchaseController {
    private static final Logger LOG = LoggerFactory.getLogger(PurchaseController.class);
    private final PurchaseService service;
    private final PurchaseMapper purchaseMapper;


    public PurchaseController(PurchaseService service, PurchaseMapper mapper) {
        this.service = service;
        this.purchaseMapper = mapper;
    }

       @GetMapping("/purchase")
    public Collection<PurchaseDto> getPurchases() {
        LOG.info("ENTER: PurchaseController#getPurchases()");
        Collection<Purchase> purchases = service.findAllPurchases();
        Collection<PurchaseDto> dtoList = purchaseMapper.mapList(purchases);
        return dtoList;
    }

    @GetMapping("/purchase/{id}")
    public PurchaseDto getPurchase(@PathVariable("id") Long id) {
        LOG.info("ENTER: PurchaseController#getPurchase(" + id + ")");

        Optional<Purchase> optPurchase = service.findPurchaseById(id);
        if (optPurchase.isPresent()) {
            return purchaseMapper.map(optPurchase.get());
        } else {
            return null;
        }
    }

    @PostMapping("/purchase")
    public PurchaseDto savePurchase(@RequestBody @Valid PurchaseDto dto) {
        LOG.info("ENTER: PurchaseController#savePurchase(" + dto + ")");
        Purchase entity = purchaseMapper.map(dto);
        Purchase persisted = service.insertPurchase(entity);
        return purchaseMapper.map(persisted);
    }

    @PutMapping("/purchase/{id}")
    public PurchaseDto putPurchase(
            @PathVariable("id") Long id,
            @RequestBody @Valid PurchaseDto dto
    ) {
        LOG.info("ENTER: PurchaseController#putPurchase(" + id + "," + dto + ")");
        Purchase entity = purchaseMapper.map(dto);

        Optional<Purchase> optPurchase = service.updatePurchase(id, entity);
        if (optPurchase.isPresent()) {
            return purchaseMapper.map(optPurchase.get());
        } else {
            return null;
        }
    }

    @DeleteMapping("/purchase/{id}")
    public void deletePurchase(@PathVariable Long id) {
        LOG.info("ENTER: PurchaseController#deletePurchase(" + id + ")");
        service.deletePurchaseById(id);
    }

}
