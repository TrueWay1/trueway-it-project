// Flavio Heim

package org.fhnw.trueway.api.controller;

import org.fhnw.trueway.business.service.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("image")
public class ImageController {
    private static final Logger LOG = LoggerFactory.getLogger(ImageController.class);

    private final ImageService service;

    public ImageController(ImageService service) {
        this.service = service;
    }

    @GetMapping(value = "/{code}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] findImageByCode(@PathVariable String code) {
        LOG.info("ENTER: ImageController#findImageByCode(" + code + ")");
        return service.findImageByProductCode(code);
    }

}
