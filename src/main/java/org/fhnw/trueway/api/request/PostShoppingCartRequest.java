// Nicolai Zaccariello
package org.fhnw.trueway.api.request;

public class PostShoppingCartRequest {
    private final Long productId;
    private final Integer quantity;

    public PostShoppingCartRequest(Long productId, Integer quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public Integer getQuantity() {
        return quantity;
    }
}
