// Gazmend Shefiu
package org.fhnw.trueway.utils;

import org.fhnw.trueway.domain.Positionable;
import org.fhnw.trueway.domain.Purchase;
import org.fhnw.trueway.domain.ShoppingCart;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ShippingCalculator {

    /**
     * key => cost category in km
     * value => price list each pallets
     */
    private static final Map<BigDecimal, double[]> KM_COSTS_MAP = new HashMap<>();

    static { // TODO: Think about what should happen when we need more then 12 pallets
        KM_COSTS_MAP.put(BigDecimal.valueOf(30), new double[] {58.65, 87.00, 114.50, 137.10, 160.80, 181.65, 201.45, 220.30, 238.35, 255.35, 271.45, 286.55});
        KM_COSTS_MAP.put(BigDecimal.valueOf(60), new double[] {67.00, 99.40, 130.85, 156.75, 183.80, 207.55, 230.25, 251.85, 272.40, 291.90, 310.25, 327.50});
        KM_COSTS_MAP.put(BigDecimal.valueOf(90), new double[] {75.40, 111.90, 147.15, 176.35, 206.65, 233.50, 259.00, 283.35, 306.45, 328.40, 348.95, 368.40});
        KM_COSTS_MAP.put(BigDecimal.valueOf(120), new double[] {83.75, 124.30, 163.50, 195.90, 229.70, 259.45, 287.70, 314.85, 340.50, 364.75, 387.80, 409.40});
        KM_COSTS_MAP.put(BigDecimal.valueOf(150), new double[] {92.15, 136.70, 179.80, 215.50, 252.60, 285.30, 316.50, 346.30, 374.50, 401.25, 426.50, 450.35});
        KM_COSTS_MAP.put(BigDecimal.valueOf(180), new double[] {100.55, 149.20, 196.15, 235.15, 275.60, 311.25, 345.35, 377.75, 408.60, 437.75, 465.35, 491.25});
        KM_COSTS_MAP.put(BigDecimal.valueOf(210), new double[] {108.95, 161.55, 212.50, 253.90, 298.60, 337.15, 374.15, 409.30, 442.60, 474.25, 504.05, 532.15});
        KM_COSTS_MAP.put(BigDecimal.valueOf(240), new double[] {117.30, 174.05, 228.90, 274.25, 321.55, 363.15, 402.90, 440.75, 476.70, 510.75, 542.90, 573.15});
        KM_COSTS_MAP.put(BigDecimal.valueOf(270), new double[] {125.65, 186.45, 245.30, 293.90, 344.55, 389.10, 431.70, 472.20, 510.75, 547.20, 581.60, 614.05});
        KM_COSTS_MAP.put(BigDecimal.valueOf(300), new double[] {134.05, 198.85, 261.60, 313.45, 367.50, 415.00, 460.45, 503.70, 544.70, 583.65, 620.35, 655.05});
        KM_COSTS_MAP.put(BigDecimal.valueOf(330), new double[] {142.40, 211.30, 277.95, 332.95, 390.45, 441.05, 489.20, 535.20, 578.85, 620.15, 659.20, 695.95});
        KM_COSTS_MAP.put(BigDecimal.valueOf(360), new double[] {150.75, 223.80, 294.30, 352.65, 413.45, 466.90, 518.00, 566.70, 612.85, 656.65, 697.95, 736.90});
    }


    public static double calculateShippingCost(Purchase purchase) {
        int numberOfPallets = calculatePallets(purchase);
        BigDecimal costCategory = calculateCostCategory(purchase.getUser().getDistance());
        return KM_COSTS_MAP.get(costCategory)[numberOfPallets];
    }

    public static double calculateShippingCost(ShoppingCart shoppingCart) {
        int numberOfPallets = calculatePallets(shoppingCart);
        BigDecimal costCategory = calculateCostCategory(shoppingCart.getUser().getDistance());
        return KM_COSTS_MAP.get(costCategory)[numberOfPallets];
    }


    /**
     *
     * @param purchase purchase with purchasePositions
     * @return number of pallets
     */
    public static int calculatePallets(Purchase purchase) {
        return getResult(purchase.getPurchasePositions());
    }

    public static int calculatePallets(ShoppingCart shoppingCart) {
        return getResult(shoppingCart.getShoppingCartPositions());
    }

    private static int getResult(Collection<? extends Positionable> purchasePositions) {
        if (null == purchasePositions || purchasePositions.isEmpty())  return 0;

        if (purchasePositions.size() == 1) {
            // Regel 1
            Positionable position = purchasePositions.stream().findFirst().get();
            int quantity = position.getQuantity();
            int maxNumberOfProducts = position.getProduct().getMaxNumberOfProducts();
            BigDecimal minPalletSpaces = position.getProduct().getMinPalletSpaces().setScale(2);
            BigDecimal factorRounded = BigDecimal.valueOf(quantity).divide(BigDecimal.valueOf(maxNumberOfProducts), 0, RoundingMode.UP);
            return minPalletSpaces.multiply(factorRounded).setScale(0, RoundingMode.UP).intValue();
        } else {
            BigDecimal sumPalletSpaces = BigDecimal.ZERO;

            for (Positionable position : purchasePositions) {
                BigDecimal quantity = BigDecimal.valueOf(position.getQuantity()).setScale(2);
                BigDecimal minPalletSpaces = position.getProduct().getMinPalletSpaces().setScale(2);
                BigDecimal maxNumberOfProducts = BigDecimal.valueOf(position.getProduct().getMaxNumberOfProducts()).setScale(2);
                BigDecimal space = minPalletSpaces.divide(maxNumberOfProducts, 2, RoundingMode.HALF_UP).multiply(quantity);
                sumPalletSpaces = sumPalletSpaces.add(space);
            }

           return sumPalletSpaces.setScale(0, RoundingMode.UP).intValue();
        }
    }

    static BigDecimal calculateCostCategory(BigDecimal distance) {
        List<BigDecimal> costCategories = KM_COSTS_MAP.keySet().stream()
                .sorted()
                .collect(Collectors.toList());

        for (BigDecimal category : costCategories) {
            if (distance.compareTo(category) <= 0) {
                return category;
            }
        }

        return CollectionUtils.lastElement(costCategories);
    }

}
