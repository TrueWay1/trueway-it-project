// Davide Seabra

package org.fhnw.trueway.business.mapper;


import org.fhnw.trueway.domain.Purchase;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = PurchasePositionServiceMapper.class)
public interface PurchaseServiceMapper {
    void populate(@MappingTarget Purchase target, Purchase source);
}
