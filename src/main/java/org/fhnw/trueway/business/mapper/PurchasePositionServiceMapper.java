// Davide Seabra

package org.fhnw.trueway.business.mapper;


import org.fhnw.trueway.domain.PurchasePosition;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.Collection;

@Mapper(componentModel = "spring", uses = ProductServiceMapper.class)
public interface PurchasePositionServiceMapper {

    void populate(@MappingTarget Collection<PurchasePosition> target, Collection<PurchasePosition> source);

    void populate(@MappingTarget PurchasePosition target, PurchasePosition source);
}
