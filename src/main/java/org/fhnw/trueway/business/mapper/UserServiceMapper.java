// Davide Seabra

package org.fhnw.trueway.business.mapper;


import org.fhnw.trueway.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = AddressServiceMapper.class)
public interface UserServiceMapper {

    @Mapping( target = "shoppingCart", ignore = true )
    @Mapping( target = "purchases", ignore = true )
    void populate(@MappingTarget User target, User source);
}
