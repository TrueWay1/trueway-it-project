// Davide Seabra

package org.fhnw.trueway.business.mapper;


import org.fhnw.trueway.domain.Address;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = PurchaseServiceMapper.class)
public interface AddressServiceMapper {

    void populate(@MappingTarget Address target, Address source);
}
