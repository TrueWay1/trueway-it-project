// Davide Seabra

package org.fhnw.trueway.business.mapper;


import org.fhnw.trueway.domain.Product;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = PurchasePositionServiceMapper.class)
public interface ProductServiceMapper {

    void populate(@MappingTarget Product target, Product source);
}
