// Davide Seabra

package org.fhnw.trueway.business.service;

import org.fhnw.trueway.business.mapper.PurchaseServiceMapper;
import org.fhnw.trueway.domain.Purchase;
import org.fhnw.trueway.infra.repository.PurchaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PurchaseService {
    private static final Logger LOG = LoggerFactory.getLogger(PurchaseService.class);

    private final PurchaseRepository repository;
    private final PurchaseServiceMapper mapper;

    public PurchaseService(PurchaseRepository repository, PurchaseServiceMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<Purchase> findAllPurchases() {
        LOG.debug("ENTER: PurchaseService#findAllPurchases()");
		return repository.findAll();
}


    public Optional<Purchase> findPurchaseById(Long id) {
        LOG.debug("ENTER: PurchaseService#findPurchaseById(" + id + ")");
        return repository.findById(id);
    }

    public Purchase insertPurchase(Purchase entity) {
        LOG.debug("ENTER: PurchaseService#insertPurchase(" + entity + ")");
        return repository.save(entity);
    }

    public Optional<Purchase> updatePurchase(Long id, Purchase entity) {
        LOG.debug("ENTER: PurchaseService#updatePurchase(" + id + "," + entity +")");

        Optional<Purchase> optPurchase = findPurchaseById(id);
        if (optPurchase.isPresent()) {
            Purchase foundPurchase = optPurchase.get();
            mapper.populate(foundPurchase, entity);
            return Optional.of(foundPurchase);
        } else {
            return Optional.empty();
        }
    }

    public void deletePurchaseById(Long id) {
        LOG.debug("ENTER: PurchaseService#deletePurchaseById(" + id + ")");
        repository.deleteById(id);
    }

}
