// Davide Seabra

package org.fhnw.trueway.business.service;

import org.fhnw.trueway.business.mapper.ProductServiceMapper;
import org.fhnw.trueway.domain.Product;
import org.fhnw.trueway.infra.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service // Eine Spring Bean
@Transactional
public class ProductService {
	private static final Logger LOG = LoggerFactory.getLogger(ProductService.class);

	private final ProductRepository repository;
	private final ProductServiceMapper mapper;


	public ProductService(ProductRepository repository, ProductServiceMapper mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}


	public List<Product> findAllProducts() {
		LOG.debug("ENTER: ProductService#findAllProducts()");
		List<Product> products = repository.findAll();
		return products;
	}


	public Optional<Product> findProductById(Long id) {
		LOG.debug("ENTER: ProductService#findProductById(" + id + ")");
		return repository.findById(id);
	}

	public Product insertProduct(Product entity) {
		LOG.debug("ENTER: ProductService#insertProduct(" + entity + ")");
		return repository.save(entity);
	}

	public Optional<Product> updateProduct(Long id, Product entity) {
		LOG.debug("ENTER: ProductService#updateProduct(" + id + "," + entity +")");
		Optional<Product> optProduct = findProductById(id);

		if (optProduct.isPresent()) {
			Product foundProduct = optProduct.get();
			mapper.populate(foundProduct, entity);
			return Optional.of(foundProduct);
		} else {
			return Optional.empty();
		}
	}

	public void deleteProductById(Long id) {
		LOG.debug("ENTER: ProductService#deleteProductById(" + id + ")");
		repository.deleteById(id);
	}

}
