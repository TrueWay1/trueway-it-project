// Davide Seabra

package org.fhnw.trueway.business.service;

import org.fhnw.trueway.domain.Product;
import org.fhnw.trueway.domain.ShoppingCart;
import org.fhnw.trueway.domain.ShoppingCartPosition;
import org.fhnw.trueway.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class ShoppingCartService {
    private static Logger LOG = LoggerFactory.getLogger(ShoppingCartService.class);

    private final ProductService productService;
    private final UserService userService;

    public ShoppingCartService(ProductService productService, UserService userService) {
        this.productService = productService;
        this.userService = userService;
    }

    public ShoppingCart addNewShoppingCartPos(Long userId, Long productId, Integer quantity) {
        LOG.debug("ENTER: ShoppingCartService#addNewShoppingCartPos(" + userId + "," + productId + "," + quantity +")");

        Optional<User> optUser = userService.findUserById(userId);
        if (optUser.isPresent()) {
            User user = optUser.get();
            ShoppingCart foundShoppingCart = user.getShoppingCart();
            Optional<Product> optProduct = productService.findProductById(productId);

            if (optProduct.isPresent()) {
                Product product = optProduct.get();
                ShoppingCartPosition position = new ShoppingCartPosition(product, quantity);
                position.setProduct(product);
                foundShoppingCart.getShoppingCartPositions().add(position);
            }
        }

        return findUserShoppingCart(userId);
    }

    private ShoppingCart findUserShoppingCart(Long userId) {
        LOG.debug("ENTER: ShoppingCartService#findUserShoppingCart(" + userId +")");

        Optional<User> optUser = userService.findUserById(userId);
        if (optUser.isPresent()) {
            return optUser.get().getShoppingCart();
        } else {
            return null;
        }
    }

}
