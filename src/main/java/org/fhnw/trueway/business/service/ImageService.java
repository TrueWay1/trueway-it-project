// Davide Seabra

package org.fhnw.trueway.business.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class ImageService {
    private static final Logger LOG = LoggerFactory.getLogger(ImageService.class);

    @Value("${app.path.image}")
    private FileSystemResource imagePath;


    public byte[] findImageByProductCode(String code) {
        LOG.debug("ENTER: ImageService#findImageByProductCode(" + code + ")");

        try {
            String absolutePath = imagePath.getFile().getAbsolutePath();
            Path fullPath = Path.of(absolutePath, code  + ".jpg");
            return Files.readAllBytes(fullPath);
        } catch (IOException e) {
            LOG.warn("Cannot find image " + code + ".jpeg", e);
            return null;
        }
    }

}
