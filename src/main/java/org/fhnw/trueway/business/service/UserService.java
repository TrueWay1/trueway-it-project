// Davide Seabra

package org.fhnw.trueway.business.service;


import org.fhnw.trueway.business.mapper.UserServiceMapper;
import org.fhnw.trueway.domain.User;
import org.fhnw.trueway.infra.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {
    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    private final UserRepository repository;
    private final UserServiceMapper mapper;

    public UserService(UserRepository repository, UserServiceMapper mapper){
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<User> findAllUser(){
        LOG.debug("ENTER: UserService#findAllUser()");
        return repository.findAll();
    }

    public Optional<User> findUserById(Long id){
        LOG.debug("ENTER: UserService#findUserById(" + id + ")");
        return repository.findById(id);
    }


    public Optional<User> findUserByEmail(String email){
        LOG.debug("ENTER: UserService#findUserByEmail(" + email + ")");
        return repository.findByEmail(email);
    }

    public User insertUser(User entity) {
        LOG.debug("ENTER: UserService#insertUser(" + entity + ")");

        User savedUser = repository.save(entity);
        entity.getShoppingCart().setUser(entity);
        return savedUser;
    }

    public Optional <User> updateUser (long id, User entity){
        LOG.debug("ENTER: UserService#updateUser(" + id + "," + entity + ")");
        Optional <User> optUser = findUserById(id);

        if (optUser.isPresent()) {
            User foundUser = optUser.get();
            mapper.populate(foundUser, entity);
            return Optional.of(foundUser);
        }
        else {
            return Optional.empty();
        }
    }

    public void deleteUserById(Long id){
        LOG.debug("ENTER: UserService#deleteUserById(" + id + ")");
        repository.deleteById(id);
    }
}
