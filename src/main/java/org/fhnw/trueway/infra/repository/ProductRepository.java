// Davide Seabra

package org.fhnw.trueway.infra.repository;

import org.fhnw.trueway.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


// Spring-Data Docu anschauen

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {


}
