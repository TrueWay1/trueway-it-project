// Davide Seabra

package org.fhnw.trueway.infra.repository;

import org.fhnw.trueway.domain.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {


}
