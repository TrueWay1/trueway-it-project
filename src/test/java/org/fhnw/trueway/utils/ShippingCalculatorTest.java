// Gazmend Shefiu

package org.fhnw.trueway.utils;

import org.fhnw.trueway.domain.Purchase;
import org.fhnw.trueway.domain.PurchasePosition;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class ShippingCalculatorTest {

    @ParameterizedTest
    @MethodSource
    void testCalculateCostCategory(BigDecimal distance, BigDecimal expectedResult) {
        BigDecimal result = ShippingCalculator.calculateCostCategory(distance);
        assertThat(result).isEqualTo(expectedResult);
    }

    private static Stream<Arguments> testCalculateCostCategory() {
        return Stream.of(
                // 30km Test calculation 1 pallet
                Arguments.of(BigDecimal.valueOf(75.40), BigDecimal.valueOf(90)),

                // 60km Test calculation 1 pallet
                Arguments.of(BigDecimal.valueOf(67.00), BigDecimal.valueOf(90)),

                // 90km Test calculation 2 pallets
                Arguments.of(BigDecimal.valueOf(111.90), BigDecimal.valueOf(120)),

                // 120km Test calculation 2 pallets
                Arguments.of(BigDecimal.valueOf(124.30), BigDecimal.valueOf(150)),

                // 330km Test calculation 7 pallets
                Arguments.of(BigDecimal.valueOf(489.20), BigDecimal.valueOf(360)),
                Arguments.of(BigDecimal.valueOf(0), BigDecimal.valueOf(30)),
                Arguments.of(BigDecimal.valueOf(29), BigDecimal.valueOf(30)),
                Arguments.of(BigDecimal.valueOf(30), BigDecimal.valueOf(30)),
                Arguments.of(BigDecimal.valueOf(31), BigDecimal.valueOf(60))
        );
    }

    /*
     * Generator in case we have time to test the ShippingCost Calculator
     * Methods using Purchase as object.
     */
    @SuppressWarnings("unused")
    static class PurchaseGenerator {

        static Purchase createPurchaseWithTwoPosition() {
            Purchase entity = new Purchase();
            entity.getPurchasePositions().add(createPurchasePosition());
            entity.getPurchasePositions().add(createPurchasePosition());
            return entity;
        }

        static Purchase createPurchaseWithOnePosition() {
            Purchase entity = new Purchase();
            entity.getPurchasePositions().add(createPurchasePosition());
            return entity;
        }

        private static PurchasePosition createPurchasePosition() {
            PurchasePosition entity = new PurchasePosition();
            // Fill Attributes
            return entity;
        }
    }

}
